/**********************************************************************************
 * Name: Clifford Dunn
 * Class: CS 344 - Operating Systems
 * Assignment: Program 2 - Adventure Game
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>

#ifndef NUM_ROOMS
#define NUM_ROOMS 7
#endif

// Create the mutex for multithreading
pthread_mutex_t mutex;

typedef struct Room Room;
struct Room
{
    const char* roomType;
    const char* roomName;
    int maxConns;
    int numConns;
    struct Room* roomConns[NUM_ROOMS];
};



/**********************************************************************************
**********************************************************************************/
Room* CreateRoom(const char* rName)
{
    struct Room* newRoom = malloc(sizeof(Room));
    assert(newRoom != NULL);

    newRoom->roomName = rName;
    newRoom->maxConns = NUM_ROOMS - 1;
    newRoom->numConns = 0;

    return newRoom;
}
/**********************************************************************************
 * Function: GetDirectory
 * Input: None
 * Output: The string name of the newest directory
 * Description: Borrowed heavily from Professor Brewster's writing on the topic
 *              This function will check for directories that exist in the current
 *              directory that have the same prefix, dunncli.room. The function
 *              will open and retreive the stat info from each directory and 
 *              return the newest directory's string name to the user.
**********************************************************************************/
char* GetDirectory()
{
    // Initialize the newestDirTime variable to a negative number. This insures
    // that all future directories will be newer.
    int newestDirTime = -1;
    // The target for the prefex string for directories
    char targetDirPre[32] = "dunncli.rooms.";
    // String buffer for the directory name
    char* newestDirName = malloc(sizeof(char) * 256);
    // Set the memory for the name to the string terminator
    memset(newestDirName, '\0', sizeof(newestDirName));

    DIR* checkDir;
    // struct dirent is for entries in the directory.
    struct dirent* fileInDir;
    struct stat dirAttributes;

    // Open the current directory
    checkDir = opendir(".");

    // While the directory isn't empty, check the directory for 
    // files that match the targetDirPre
    if(checkDir > 0)
    {
        while((fileInDir = readdir(checkDir)) != NULL)
        {
            if(strstr(fileInDir->d_name, targetDirPre) != NULL)
            {
                // Place the statistics from the directory into
                // the dirAttributes stat structure 
                stat(fileInDir->d_name, &dirAttributes);

                // Check if the time this directory was created is
                // newer than the time of the previous newest directory.
                // If it is, place the name of this new directory into the
                // string dedicated to the newest directory.
                if((int)dirAttributes.st_mtime > newestDirTime)
                {
                    newestDirTime = (int)dirAttributes.st_mtime;
                    memset(newestDirName, '\0', sizeof(newestDirName));
                    strcpy(newestDirName, fileInDir->d_name);
                }
            }
        }
    }
    
    // Close the current directory
    closedir(checkDir);
    // Return the name of the newest directory to the user
    return newestDirName;

}

/**********************************************************************************
 * Function: FindFileName
 * Input: A directory to search
 * Output: The name of the current file in the directory
 * Description: This function searches through the given directory and will return
 *              the first name of the first file found.
**********************************************************************************/
char* FindFileName(DIR* directory)
{
    // struct dirent to find specific files
    struct dirent* currFile;
    int i=0;

    // While loop looks through the directory. 
    // currFile is the next file found from readdir.
    // As long as the directory is open, this will point
    // to the next available file.
    while(currFile = readdir(directory))
    {
        // On UNIX systems, the directories have the '.' and '..'
        // files that are actually symlinks to the current or parent
        // directory. When these files are encountered, they should be
        // ignored
        if(!strcmp(currFile->d_name, "."))
            continue;
        if(!strcmp(currFile->d_name, ".."))
            continue;

        // Break the while loop at the first suitable file.
        break;



    }

    // Return the name of this file to the user.
    return currFile->d_name;
}


/**********************************************************************************
 * Function: FindRoomName
 * Input: The file corresponding to the room
 * Output: The char string of the name of the room
 * Description: This function looks for specific string tokens in the open file
 *              When the correct string is found, 
**********************************************************************************/
char* FindRoomName(FILE* file)
{
    // Line buffer to store char data from the file
    char line[50];
    // Create a string buffer to hold the file name
    char* strBuf = malloc(sizeof(char) * 60);

    // Get the next line in the file.
    fgets(line, sizeof(char) * 50, file);
    // strtok puts all the characters until a certain
    // token is found into theroomName variable.
    // Each subsequent call overwrites the variable with new char data
    char* roomName = strtok(line, " ");
    roomName = strtok(NULL, " ");
    roomName = strtok(NULL, "\n");

    // Copy the char* created from room name into the string buffer
    // and return that buffer to the user.
    strcpy(strBuf, roomName);

    return strBuf;

}

/**********************************************************************************
 * Function: GetRoomByName
 * Input: The array of rooms, and the name of the room to find
 * Output: Returns the room that is sought
 * Description: This is a useful Get function that returns the room based on the
 *              name being sought. Requires an existing array of rooms.
**********************************************************************************/
Room* GetRoomByName(struct Room* rooms[], char* rName)
{
    int i;
    for(i = 0; i < NUM_ROOMS; i++)
    {
        // If the input character string matches the name of a room,
        // return the room to the user.
        if(strcmp(rName, rooms[i]->roomName) == 0)
        {
            return rooms[i];
        }
    }

    return NULL;

}


/**********************************************************************************
 * Function: FindRoomConnection
 * Input: The file corresponding to the room. The number of the connection to find.
 * Description: This function will search the file and return the name of the nth
 *              desired room connection.
**********************************************************************************/
char* FindRoomConnection(FILE* file, int n)
{
    
    char line[50];
    char* strBuf = malloc(sizeof(char) * 24);
    // Convert the integer entered into an ASCII char for comparison.
    char c;  
    c = n + '0';

    // While loop searches the entire file looking for the correct
    // text data to correspond to the right room name.
    while(fgets(line, sizeof(char) * 50, file) != NULL)
    {
        char* connection = strtok(line, " ");
        // While loop will grab lines until it finds the word 
        // CONNECTION. When this string of chars is found, the
        // if function will be called.
        if(strcmp(connection, "CONNECTION") == 0)
        {
            // step to the next white space.
            connection = strtok(NULL, " ");
            // We are looking for the corresponding character that
            // matches the integer for the connection we seek. 
            // This is a straight == statement.
            if(connection[0] == c)
            {
                // Once we find the proper connection, we place
                // the char* into the string buffer and return to the user
                connection = strtok(NULL, "\n");
                strcpy(strBuf, connection);
                return strBuf;
            }
        }
    }
        
    // If the function doesn't find anything, return NULL.
    return NULL;
}

/**********************************************************************************
 * Function: FindRoomType
 * Input: The corresponding room's file
 * Output: Returns the char* represnting the room type (START, MID, END)_ROOM
 * Description: Like the previous two functions, the file will be read and search
 *              for a specific set of chars in order to find the proper room type
 *              and return it to the user.
**********************************************************************************/
char* FindRoomType(FILE* file)
{
    char line[50];
    char* strBuf = malloc(sizeof(char) * 60);

    // While loop searches the entire file looking for the correct
    // text data to correspond to the right room name.
    while(fgets(line, sizeof(char) * 50, file) != NULL)
        {
            char* connection = strtok(line, " ");
            // This time we are looking for ROOM
            if(strcmp(connection, "ROOM") == 0)
            {
                connection = strtok(NULL, " ");
                // Previously in the file, we would encounter ROOM NAME:
                // This time we need ROOM TYPE:
                if(strcmp(connection, "TYPE:") == 0)
                {
                    // When we've found the right room type, we copy the
                    // chars into the string and return to the user.
                    connection = strtok(NULL, "\n");
                    strcpy(strBuf, connection);
                    return strBuf;
                }
            }
        }
}


/**********************************************************************************
 * Function: GetRoomType
 * Input: A room Structure
 * Output: The char* of the room type.
 * Description: Simple Get function. Initial compiler warning pressed me to insure
 *              that the returned char is unsigned.
**********************************************************************************/
unsigned char* GetRoomType(struct Room* room)
{
    return (unsigned char *)room->roomType;
}

/**********************************************************************************
 * Function: GetRoomName
 * Input: A room Structure
 * Output: The char* of the room name.
 * Description: Simple Get function. Initial compiler warning pressed me to insure
 *              that the returned char is unsigned.
**********************************************************************************/
unsigned char* GetRoomName(struct Room* room)
{
    return (unsigned char *)room->roomName;
}

/**********************************************************************************
 * Function: GetRoomNumConns
 * Input: A room Structure
 * Output: An int with the number of connections currently contained in this room.
 * Description: Simple Get function.
**********************************************************************************/
int GetRoomNumConns(struct Room* room)
{
    return room->numConns;
}


/**********************************************************************************
 * Function: GetStartRoom
 * Input: The array of rooms
 * Output: The initial room in which to start
 * Description: Finding the start room is important for game function. Because
 *              The rooms are shuffled and randomized before the program is run,
 *              the computer can quickly find the start room by cycling through
 *              all the rooms and calling the GetRoomType function. This will
 *              stop when the start room is fount.
**********************************************************************************/
struct Room* GetStartRoom(struct Room* rooms[])
{
    int i;
    for(i=0; i < NUM_ROOMS; i++)
    {
        if(strcmp(GetRoomType(rooms[i]), "START_ROOM") == 0)
            return rooms[i];
    }

}

/**********************************************************************************
 * Function: GetConnectedRoom
 * Input: A room, and the number of the desired connected room.
 * Output: The connected room will be returned to user.
 * Description: The user can input the current room, and the number of the desired
 *              connected room based on the index of roomConns. This will return
 *              the room which is connected to the current room.
**********************************************************************************/
struct Room* GetConnectedRoom(struct Room* room, int connNum)
{
    return room->roomConns[connNum];

}


/**********************************************************************************
 * Function: IsRoom
 * Input: The array of rooms, the name of a candidate room.
 * Output: Simple boolean output where a 1 is true and a 0 is false
 * Description: Because the user can type jibberish into the console, the system
 *              needs to validate input to make sure that a room exists by the 
 *              desired name.
**********************************************************************************/
int IsRoom(struct Room* rooms[], char* rName)
{
    int i;
    for(i=0; i < NUM_ROOMS; i++)
    {
        if(strcmp(rName, GetRoomName(rooms[i])) == 0)
            return 1;
    }
            return 0;

}


/**********************************************************************************
 * Function: IsConnected
 * Input: Two pointers to two rooms
 * Output: Essentially a bool. Returns a 1 for true and a 0 for false
 * Description: Initial logic places whichever room has the highest maxConns in
 *              a variable called end. For this assignment, all rooms have the
 *              same maxConns. Then a for loop checks every connection in both
 *              rooms and makes sure that the rooms are not connected to each
 *              other. If they are connected, returns true. Otherwise return false
**********************************************************************************/
int IsConnected(Room* x, Room* y)
{
    int i=0;
    int end;

    if(x->maxConns < y->maxConns)
        end = y->maxConns;
    else
        end = x->maxConns;
    
    for(i; i < end; i++)
    {
        if(x->roomConns[i] == y || y->roomConns[i] == x)
            return 1;
    }

    return 0;

}


/**********************************************************************************
 * Function: WriteTime
 * Input: None
 * Output: A text file will be written into the current directory
 * Description: A void pointer is necessary for the mutex and POSIX thread. 
 *              This function will create a file and post the current time stamp
 *              into the file. Then it will close the file.
**********************************************************************************/
void* WriteTime()
    // void* function required for pthread use
{
    FILE* fp;
    fp = fopen("currentTime.txt", "w+");
    char timePrint[64];
    // struct tm is the time structure. 
    struct tm* sTM;

    // Current time on the system.
    time_t curr = time(0);
    // Convert the time to local time (OSU Server in PST)
    sTM = localtime(&curr);

    // Convert the time format to the desired format for output.
    strftime(timePrint, sizeof(char) * 64, "%I:%M%p, %A, %B %d, %Y\n", sTM);
    
    // Place the time into the file and close it.
    fputs(timePrint, fp);
    fclose(fp);
} 


/**********************************************************************************
 * Function: PrintTimeFromFile
 * Input: None
 * Output: Prints the timestamp from the currentTime.txt file
 * Description: Requires the currentTime.txt file. Will print its contents to the
 *              screen.
**********************************************************************************/
void PrintTimeFromFile()
{
    char timeString[64];
    FILE* fp;
    fp = fopen("currentTime.txt", "r");

    fgets(timeString, sizeof(char)*64, fp);
    printf("\n%s\n", timeString);
    fclose(fp);
}


/**********************************************************************************
 * Function: PThread2
 * Input: None
 * Output: None directly, but will call the void* function WriteTime.
 * Description: This function requires the mutex that was initilized at the
 *              beginning of the program. The function creates a second processor
 *              thread in order to calculate the time and write the time to a file
 *              Much help was found on POSIX threads at: 
 *              https://computing.llnl.gov/tutorials/pthreads/
**********************************************************************************/
void PThread2()
{
    // Initializing the thread and locking the mutex
    pthread_t secondThread;
    pthread_mutex_init(&mutex, NULL);
    pthread_mutex_lock(&mutex);

    // Writes the time to file. IMPORTANT! Calling the WriteTime function
    // is to be done without the () terminator.
    int time = pthread_create(&secondThread, NULL, WriteTime, NULL);
    // Unlocks then destroys the mutex for the time function.
    pthread_mutex_unlock(&mutex);
    pthread_mutex_destroy(&mutex);

    // usleep prevents a seg fault from calling the PrintTimeFromFile function
    // before the file exists.
    usleep(50);

}





/**********************************************************************************
 * Function: PlayGame
 * Input: The array of rooms, the name of the initial room in which to start
 * Output: Game time!
 * Description: This is the primary function for the interaction of the game 
 *              system. This works once all proper data from files have been loaded
 *              into memory and can be quickly accessed. 
**********************************************************************************/
void PlayGame(struct Room* rooms[], char* roomName)
{
    // Counter variables
    int i, steps = 0, end = 0;
    // User will be prompted for inputs for the next room
    char newRoomName[50];
    // The path is a large buffer to hold all the chars of all the places the
    // player has been
    char* path[1024];
    // The struct Room* will be used to find the room for which to play. 
    // This room will be found based on the input of roomName.
    struct Room* room = GetRoomByName(rooms, roomName);

    // do while loop ensures the initial beginning. Though this will always
    // begin because a room can only be one type and we always start with the 
    // START_ROOM
    do
    {
        // Visual Output of the data
        printf("CURRENT LOCATION: %s\n", GetRoomName(room));
        printf("POSSIBLE CONNECTIONS: ");
        for(i=0; i < GetRoomNumConns(room)-1; i++)
        {
            printf("%s, ", GetRoomName(GetConnectedRoom(room, i)));
        }
        printf("%s.\n", GetRoomName(GetConnectedRoom(room, GetRoomNumConns(room)-1)));
        printf("WHERE TO? >");

        // User inputs the new name of a room
        scanf("%s", newRoomName);


        // Check to see if the user is actually calling for the time function. If so,
        // execute the two necessary functions to print the time.
        if(strcmp(newRoomName, "time") == 0)
        {
            PThread2();
            PrintTimeFromFile();
        }
        
        // Otherwise, check to see if a room was actually entered. If a room doesn't exist
        // by the entered name, give an error
        else if(IsRoom(rooms, newRoomName) == 0)
                printf("\nHUH? I DON'T UNDERSTAND THAT ROOM. TRY AGAIN.\n\n");

        // If the user enters a valid room name, but it is a room that is not currently connected, 
        // give an error message.
        else if(IsRoom(rooms, newRoomName) == 1 && IsConnected(room, GetRoomByName(rooms, newRoomName)) == 0)
                printf("\nHUH? I DON'T UNDERSTAND THAT ROOM. TRY AGAIN.\n\n");
                
        // Finally, we learn that the user enters the right information. The path variable receives the 
        // name of the room the user is now leaving. The steps counter increments, and the current room
        // changes to the new room selected.
        else if(IsRoom(rooms, newRoomName) == 1 && IsConnected(room, GetRoomByName(rooms, newRoomName)) == 1)
        {
            path[steps] = GetRoomName(room);
            steps++;
            room = GetRoomByName(rooms, newRoomName);
            printf("\n");

        }

            // If the END_ROOM type is encountered, the end variable will change. This will exit
            // the While loop and end the game.
            if(strcmp(GetRoomType(room), "END_ROOM") == 0)
            {
                path[steps] = GetRoomName(room);
                end = 1;
            }

    }
    while(end == 0);

    // Prints out all the relevent end of game data.
    printf("YOU HAVE FOUND THE END ROOM. CONGRATULATIONS!\n");
    printf("YOU TOOK %d STEPS. YOUR PATH TO VICTORY WAS:\n", steps);
    for(i=0; i <= steps; i++)
    {
        printf("%s\n", path[i]);
    }

}



/**********************************************************************************
 * Function: main
 * Input: None
 * Output: Everything!
**********************************************************************************/
int main()
{

    // Execute the second thread. Will insure accurate time keeping.
    PThread2();
    // Variables that will primarily hold all the room data
    int i, j;
    Room* rooms[NUM_ROOMS];
    const char* fileNames[NUM_ROOMS];
    char* roomNames[NUM_ROOMS];
    char* allRoomConns[NUM_ROOMS][NUM_ROOMS];;
    char* roomTypes[NUM_ROOMS];
    char* directory = GetDirectory();
    // The following variables will be necessary for directory and file
    // reading
    FILE* roomFiles[NUM_ROOMS];
    DIR* dir = opendir(directory);
    char fullFile[100];
    FILE* fp;

/**********************************************************************************
 * This for loop exists inside of main. Initially, this was intended to be its own
 * function. However, I had massive problems with fopen causing seg faults and
 * output of garbage while it existed in functions. Being in a large for loop like
 * this eliminated those problems.
**********************************************************************************/
    // For loop cycles through the time of NUM_ROOMS. This insures that all
    // rooms are created and they have the same quantity of data.
    for(i=0; i < NUM_ROOMS; i++)
    {

        
        // First we find all the file names for the rooms. They will be
        // placed in an array for future reference.
        fileNames[i] = FindFileName(dir);
        sprintf(fullFile, "./%s/%s", directory, fileNames[i]);
        fp = fopen(fullFile, "r");
        // Once the names from the files are identified, we find the
        // names of the rooms from those files.
        roomNames[i] = FindRoomName(fp); 
        fclose(fp);
        // Close the file when done.
        // Create all the rooms from the files.
        rooms[i] = CreateRoom(roomNames[i]);

       
        // Open the files again and cycle through.
        fp = fopen(fullFile, "r");
        // This loop populates the map. All connections are placed in a 2D
        // array. This array corresponds to a [room][roomConns]. This is
        // filled with char* strings.
        for(j=0; j< NUM_ROOMS; j++)
        {
                allRoomConns[i][j] = FindRoomConnection(fp, (j+1));
        }
        fclose(fp);
        // Close the file when done.
        
        // Open the file again and find all the file types.
        fp = fopen(fullFile, "r");
        roomTypes[i] = FindRoomType(fp);
        rooms[i]->roomType = roomTypes[i];
        fclose(fp);
    }

 
    // This double for loop is necessary to place all the connections to and 
    // from rooms. Without being in its own for loop, seg faults were common
    // occurances. Going between 1D and 2D arrays is sometimes confusing.
    for(i=0; i < NUM_ROOMS; i++)
    {
        for(j = 0; j < NUM_ROOMS; j++)
        {
            if(allRoomConns[i][j] != NULL)
            {
               rooms[i]->roomConns[j] = GetRoomByName(rooms, allRoomConns[i][j]); 
               rooms[i]->numConns++;
            }
        }
    }

/**********************************************************************************
 * After the for loops, the game is called
**********************************************************************************/


    struct Room* startRoom = GetStartRoom(rooms);
    PlayGame(rooms, GetRoomName(startRoom));
    

    // Free memory
    closedir(dir);

    free(directory);
    for(i=0; i < NUM_ROOMS; i++)
    {
        for(j=0; j < NUM_ROOMS; j++)
        {
            free(allRoomConns[i][j]);
        }
        free(roomNames[i]);
        free(roomTypes[i]);
        free(rooms[i]);
    }

    return 0;

}
