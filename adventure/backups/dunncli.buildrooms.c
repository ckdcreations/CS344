/**********************************************************************************
 * Name: Clifford Dunn
 * Class: CS 344 - Operating Systems
 * Assignment: Program 2 - Adventure Rooms
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <time.h>


// I am fond of defining macros with the #ifndef because then 
// the compiler can be used to change the value of these macros
#ifndef NUM_ROOMS
#define NUM_ROOMS 7
#endif



const char* RoomNames[10] =
{
    "Winterfell",       // Ancestral home to House Stark
    "Casterly_Rock",    // Ancestral home to House Lannister
    "Highgarden",       // Ancestral home to House Tyrell
    "The_Eyrie",        // Ancestral home to House Arryn
    "Riverrun",         // Ancestral home to House Tully
    "Sunspear",         // Ancestral home to House Martell
    "Storms_End",      // Ancestral home to House Baratheon
    "Pyke",             // Ancestral home to House Greyjoy
    "Dragonstone",      // Ancestral home to House Targaryen
    "Castle_Black"      // Ancestral fortress for the Night's Watch
};

typedef struct Room Room;
struct Room 
{
    const char* roomType;
    const char* roomName;
    int maxConns;
    int numConns;
    struct Room* roomConns[NUM_ROOMS];

};


/**********************************************************************************
 * Function: CreateRoomTypes
 * Input: An array of strings; Number of elements in array
 * Output: void
 * Description: Creates the room types. Room types are either START_ROOM, END_ROOM
 *              or MID_ROOM. Only one START_ROOM and END_ROOM are allowed. All
 *              other values will be MID_ROOM. 
**********************************************************************************/
void CreateRoomTypes(const char* roomTypes[], size_t n )
{
    roomTypes[0] = "START_ROOM";
    roomTypes[1] = "END_ROOM";

    int i=2;
    for(i; i < n; i++)
    {
        roomTypes[i] = "MID_ROOM";
    }

}


/**********************************************************************************
 * Function: StringShuffler
 * Input: An array of strings; Number of elements in array
 * Output: void
 * Description: An implementation of the Fisher-Yates shuffle algorithm. See:
 *              https://en.wikipedia.org/wiki/Fisher-Yates_shuffle
 *              This will randomize the array so each time the program is run, a
 *              different order will occur for any array of strings.
**********************************************************************************/
void StringShuffler(const char* array[], size_t n)
{
    size_t i=0;
    size_t r=0;

    for(i; i < n; i++)
    {
       r = i + rand() % (n - i);

       const char* temp = array[i];
       array[i] = array[r];
       array[r] = temp;
    }
}


/**********************************************************************************
 * Function: CreateRoom 
 * Input: A string representing roomType, string representing roomName, integer
 *        representing maxConns
 * Output: Returns a created room
 * Description: Allocates the memory for and creates a Room. Gives it the values
 *              for roomType, roomName, and maxConns that are input
**********************************************************************************/
Room* CreateRoom(const char* type, const char* name)
{    
 
    Room* room = malloc(sizeof(Room));
    assert(room != NULL);

    room->roomType = type;
    room->roomName = name;
    room->maxConns = NUM_ROOMS - 1;
    room->numConns = 0;

    return room;
}


/**********************************************************************************
 * Function: IsGraphFull
 * Input: A pointer to an array of Rooms
 * Output: Essentially a bool. Returns a 1 for true and a 0 for false
 * Description: Cycle through every room in the array of rooms. Check each room
 *              and see if it has at least three connections. If it does not, 
 *              return 0. If all rooms have at least three connections, then the 
 *              graph is full. 
**********************************************************************************/
int IsGraphFull(Room* allRooms[]) 
{
    int i=0;
    for(i; i < NUM_ROOMS; i++)
    {
        if(allRooms[i]->numConns < 3)
            return 0;
    }

    return 1;
}



/**********************************************************************************
 * Function: FillRoomList
 * Input: A pointer to an array of rooms, a pointer to an array of strings
 *        containing the room types, and an array of strings containing the room
 *        names.
 * Output: void
 * Description: This cycles through all the rooms in the array and assigns the 
 *              room type and room name straight across from the string arrays.
**********************************************************************************/
void FillRoomList(Room* allRooms[], const char* rTypes[], const char* rNames[])
{
    int i=0;
    for(i; i < NUM_ROOMS; i++)
    {
        allRooms[i] = CreateRoom(rTypes[i], rNames[i]); 
    }
}


/**********************************************************************************
 * Function: GetRandRoom
 * Input: A pointer to an array of rooms.
 * Output: A pointer to the room that was randomly selected.
 * Description: A random number is generated from 0 to NUM_ROOMS-1. The 
 *              corresponding number of the array of rooms is returned.
**********************************************************************************/
Room* GetRandRoom(Room* allRooms[])
{
    int randomRoom = rand() % NUM_ROOMS;
    return allRooms[randomRoom];

}


/**********************************************************************************
 * Function: CanConnect
 * Input: A pointer to a Room
 * Output: Essentially a bool. Returns a 1 for true and a 0 for false
 * Description: Simple if statement determines if the number of connections 
 *              already made to this room exceed the number of available rooms to 
 *              connect. If they do, returns false. If they don't, returns true.
**********************************************************************************/
int CanConnect(Room* x)
{
    //if(x->numConns < 6)
    if(x->numConns < x->maxConns)
        return 1;
    else
        return 0;
}


/**********************************************************************************
 * Function: ConnectRoom
 * Input: Two pointers to two rooms
 * Output: void
 * Description: First entered room will connect to the second entered room. The
 *              first room will look to the next open position in its array of room
 *              connections and add the second room to that connection. The total
 *              number of connections will increment by 1.
**********************************************************************************/
void ConnectRoom(Room* x, Room* y)
{
    x->roomConns[x->numConns] = y;
    x->numConns++;
}





/**********************************************************************************
 * Function: IsSameRoom
 * Input: Two pointers to two rooms
 * Output: Essentially a bool. Returns a 1 for true and a 0 for false
 * Description: Simple if statement checks if the first room entered is the same
 *              as the second room entered. If it is, returns false. Otherwise
 *              it returns true.
**********************************************************************************/
int IsSameRoom(Room* x, Room* y)
{
    if(x == y)
        return 1;
    else
        return 0;

}

/**********************************************************************************
 * Function: IsConnected
 * Input: Two pointers to two rooms
 * Output: Essentially a bool. Returns a 1 for true and a 0 for false
 * Description: Initial logic places whichever room has the highest maxConns in
 *              a variable called end. For this assignment, all rooms have the 
 *              same maxConns. Then a for loop checks every connection in both
 *              rooms and makes sure that the rooms are not connected to each 
 *              other. If they are connected, returns true. Otherwise return false
**********************************************************************************/
int IsConnected(Room* x, Room* y)
{
    int i=0;
    int end;

    if(x->maxConns < y->maxConns)
        end = y->maxConns;
    else
        end = x->maxConns;

    for(i; i < end; i++)
    {
        if(x->roomConns[i] == y || y->roomConns[i] == x)
            return 1;
    }

    return 0;

}

/**********************************************************************************
 * Function: AddRandConn
 * Input: A pointer to an array of rooms
 * Output: void
 * Description: First, two rooms are set up. Then an infinite loop is called that
 *              will select a room at random. If the randomly selected room can
 *              connect to another room, that loop will break. A second infinite
 *              loop will be called that selects another room at random. If this
 *              room can connect to another room AND it is not the same room as 
 *              the first selected room AND it is not already connected to the 
 *              first room, the loop will break. After the loops break, the first
 *              room will be connected to the second, and the second room will be
 *              connected to the first.
**********************************************************************************/
void AddRandConn(Room* allRooms[])
{
    Room* A;
    Room* B;

    while(1)
    {
        A = GetRandRoom(allRooms);
        if(CanConnect(A) == 1)
            break;
    }

    while(1)
    {
        B = GetRandRoom(allRooms);
        if(CanConnect(B) == 1 && IsSameRoom(A, B) == 0 && IsConnected(A, B) == 0)
            break;
    }

    ConnectRoom(A, B);
    ConnectRoom(B, A);
   
}


/**********************************************************************************
 * Function: PrintAllConns
 * Input: A pointer to an array of rooms
 * Output: void
 * Description: Primarily for testing purposes. This will simply print to the 
 *              command line all rooms and their connections
**********************************************************************************/
void PrintAllConns(Room* allRooms[])
{
    int i, j;
    for(i=0; i < NUM_ROOMS; i++)
    {
        printf("%s has %d connections:\n", allRooms[i]->roomName, allRooms[i]->numConns);
        for(j=0; j < NUM_ROOMS; j++)
        {
            if(allRooms[i]->roomConns[j] != NULL)
                printf("\t%d %s", j+1, allRooms[i]->roomConns[j]->roomName);
        }
        printf("\n\n");

    }
}


/**********************************************************************************
 * Function: MakeDir
 * Input: none
 * Output: The proper name for the new directory is created.
 * Description: First, the process id is identified. This will be used to 
 *              differentiate the directory that this specific execution of the 
 *              program creates. The dirName is hardcoded as instructed. The 
 *              newDir variable has a rather large malloc as a safety for future
 *              expansion. The sprintf function prints the data to a string. The
 *              newDir string is then returned to the user.
**********************************************************************************/
char* MakeDir()
{
    int pid = getpid();
    char* dirName = "dunncli.rooms.";

    char* newDir = malloc(sizeof(char) * 50);

    sprintf(newDir, "%s%d", dirName, pid);
    
    return newDir;
}


/**********************************************************************************
 * Function: WriteRoomFile
 * Input: A pointer to a room, and the directory to write the new file
 * Output: void
 * Description: First, go into the directory specified. Create the File with fopen
 *              then write all the data that is necessary as the assignment
 *              guidelines dictate. When the writing is finish, close the file.
**********************************************************************************/
void WriteRoomFile(Room* room, char* directory)
{
    chdir(directory);

    FILE* fp;

    fp = fopen(room->roomName, "w");
    fprintf(fp, "ROOM NAME: %s\n", room->roomName);

    int i=0;
    for(i; i < room->numConns; i++)
    {
        fprintf(fp, "CONNECTION %d: %s\n", i+1, room->roomConns[i]->roomName);
    }

    fprintf(fp, "ROOM TYPE: %s\0", room->roomType);
//    fprintf(fp, "\0");

    fclose(fp);

}

/**********************************************************************************
 * Function: FreeAllRooms
 * Input: A pointer to an array of rooms
 * Output: void
 * Description: This will cycle through every room which had memory allocated on
 *              the heap. This will free all the rooms thereby removing any 
 *              possibility for a memory leak
**********************************************************************************/
void FreeAllRooms(Room* allRooms[])
{
    int i=0;
    for(i; i < NUM_ROOMS; i++)
    {
        free(allRooms[i]);
    }

}




/**********************************************************************************
 * Function: main
 * Description: This is the main function. Most programmatic material lies in
 *              different functions. The main function is little more than a 
 *              series of function calls.
**********************************************************************************/
int main()
{
    // Seed the random number generator. A lot of reading reveals that
    // this is not the most secure way to generate random numbers. If 
    // this was a banking program or a program requiring heavy security,
    // a different method would be necessary.
    srand(time(0));

    // Create the array of room types based on how many rooms are defined.
    const char* RoomTypes[NUM_ROOMS];
    // Fill the array with room types
    CreateRoomTypes(RoomTypes, NUM_ROOMS);
    // Shuffle the array of room types so rooms have random type.
    StringShuffler(RoomTypes, NUM_ROOMS);
    // Shuffle the array of room names.
    StringShuffler(RoomNames, 10);

    // Create the array of rooms that will hold all the created rooms.
    struct Room* listOfRooms[NUM_ROOMS];
    // Fill the array of rooms with the shuffled room types, shuffled room names, and
    // a value for maxConns that is between 3 and 6
    FillRoomList(listOfRooms, RoomTypes, RoomNames);


    // This while loop will continue until every room has at least three connections.
    // There will be no more than six connections per room. Rooms will be selected at
    // random and connected to another room at random. 
    while(IsGraphFull(listOfRooms) == 0)
    {
        AddRandConn(listOfRooms);
    }
    
    // Create the new directory's name
    char* directory = MakeDir();
    // Create the directory itself
    mkdir(directory, 0755);

    // This for loop will cycle through all the rooms and write the relevent data
    // to appropriate files that will be placed inside the specified directory
    int i=0;
    for(i; i < NUM_ROOMS; i++)
    {
        WriteRoomFile(listOfRooms[i], directory);
    }

    // At the end of the program, free all allocated memory. 
    FreeAllRooms(listOfRooms);
    free(directory);

    return 0;
}
