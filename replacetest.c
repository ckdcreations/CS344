#include <stdio.h>
#include <string.h>

char *replace_str(char *str, char *orig, char *rep)
{
static char buffer[4096];
char *p;
int i=0;

while(str[i]){
    if (!(p=strstr(str+i,orig)))  return str;
    strncpy(buffer+strlen(buffer),str+i,(p-str)-i);
    buffer[p-str] = '\0';
    strcat(buffer,rep);
    printf("STR:%s\n",buffer);
    i=(p-str)+strlen(orig);
}

return buffer;
}

int main(void)
{
  char str[100],str1[50],str2[50];
  printf("Enter a one line string..\n");
  fgets(str, strlen(str), stdin);
  fflush(stdin);
  printf("Enter the sub string to be replaced..\n");
  fgets(str1, strlen(str1), stdin);
  fflush(stdin);
  printf("Enter the replacing string....\n");
  fgets(str2, strlen(str2), stdin);
  fflush(stdin);
  puts(replace_str(str, str1, str2));

  return 0;
}
