""" Clifford Dunn
    CS344 - Operating Systems
    Python Programming Assignment
    Compatible with all three versions of Python
"""


""" Import Libraries """
import random, string


""" Function Definitions """
def random_string(length):
    """ Generate a random string of length 'length'"""
    return ''.join(random.choice(string.ascii_lowercase) for i in range(length))


def random_number(n):
    """ Print random numbers and a running product based on the value of 'n'"""
    product = 1
    for x in range(0, n):
        randInt = random.randint(1, 42)
        print(randInt)
        product *= randInt 

    return product


""" Main Program """
for i in range(0, 3):
    randString = random_string(10)
    print(randString)
    randString += '\n'
    newFile = open("file"+str(i), "w")
    newFile.write(randString)
    newFile.close()

print(random_number(2))
