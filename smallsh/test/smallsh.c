/**********************************************************************************
 * Name: Clifford Dunn
 * Class: CS 344 - Operating Systems
 * Assignment: Program 3 - smallsh
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <signal.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>


// I am fond of defining macros with the #ifndef because then 
// the compiler can be used to change the value of these macros
#ifndef CHARS_MAX
#define CHARS_MAX 2048
#endif

#ifndef ARG_MAX
#define ARG_MAX 512
#endif


void catchSIGTSTP(int);


// Global variable sigBG is necessary to provide a switch for the SIGTSTP
int sigBG=1;


/**********************************************************************************
**********************************************************************************/
void catchSIGTSTP(int sig)
{
    if(sig == SIGTSTP)
    {
      if(sigBG == 0)
      {
          sigBG = 1;
          fprintf(stdout, "Exiting foreground-only mode\n");
          fflush(stdout);
      }
      else if(sigBG == 1)
      {
          sigBG = 0;
          fprintf(stdout, "Entering foreground-only mode (& is now ignored)\n");
          fflush(stdout);
      }
    }
}

/**********************************************************************************
 * Main Function
 * The main function is very large because almost all the necessary functions
 * exist within main. Further development would probably create more external
 * functions and function calls
**********************************************************************************/
int main()
{
 
  
/**********************************************************************************
 * int variables
 * numArgs keeps track of how many seperate arguments are entered. endLoop is used
 * to exit the while loop. bg is a switch to send commands to the background. 
 * status holds the last commands status return. inFile will be used with read()
 * outfile will be used with write(). pidLen will hold the char length of the PID
 * i is the iterator.
**********************************************************************************/
  int numArgs = 0;
  int endLoop = 0;
  int bg = 0;
  int status = 0;
  int inFile;
  int outFile;
  int pidLen;
  int i = 0;

/**********************************************************************************
 * pid_t variables
 * pid is the PID used for children. curPID is the parent PID
**********************************************************************************/
  pid_t pid;
  pid_t curPID;

/**********************************************************************************
 * char variables
 * line holds the data that the user inputs. argInputs holds the various commands
 * entered by the user. token is used for parsing string tokens. inName is used to
 * determine the input file name. outName is used to determine the output file 
 * name. pidString holds the string of the pid_t for the current PID
**********************************************************************************/
  char line[CHARS_MAX];
  char* argInputs[ARG_MAX];
  char* token;
  char* inName = NULL;
  char* outName = NULL;
  char pidString[50];


  // sigaction struct in order to catch the sigint actions.
  struct sigaction action;
  action.sa_handler = SIG_IGN;
  sigaction(SIGINT, &action, NULL);



  // Get the current PID
  curPID = getpid();
  // Convert the PID from type pid_t to a string
  sprintf(pidString, "%d", (int)curPID);
  // Get the length of the PID string (can be variable).
  pidLen = strlen(pidString);

 
  // Call the catchSIGTSTP function. Output an error if there are problems.
  if(signal(SIGTSTP, catchSIGTSTP) == SIG_ERR)
  {
      fprintf(stderr, "failed to install sighandler\n");
      fflush(stdout);
      exit(1);
  }

  
  
/**********************************************************************************
 * Primary loop
 * This is the interface for the entire shell. This loops through all inputs, it
 * controls the output display, and when this loop is finished, the program ends.
**********************************************************************************/
  while(!endLoop)
  {
      // Start of loop. Initialize foreground running. Make sure the file names
      bg = 0;
      
      // Make sure file names are initially NULL
      inName = NULL;
      outName = NULL;

      // The user prompt! I fflush() stdin and stdout.
      fprintf(stdout, ": ");
      fflush(stdin);
      fflush(stdout);

      
      // Read the user input.
      fgets(line, sizeof(line), stdin);

      // A string buffer for the input.
      char lineBuffer[CHARS_MAX] = {0};
      // Holds the point where string will be inserted
      char* insertPoint = &lineBuffer[0];
      // The token used for variable expansion
      const char* needle = "$$";
      // A copy of the original line.
      const char* tmp = line;

  
/**********************************************************************************
 * Variable expansion loop
 * This loop checks the input for instances of $$. When $$ is found, it will be
 * replaced with the current PID. The new line will then be used. The loop is 
 * modified from a needle in the haystack search.
**********************************************************************************/
      while(1)
      {
      
          //Find the next instance of the $$ token.
          const char* findNeedle = strstr(tmp, needle);

      
          // When finished with the string, place everything into insertPoint
          // which is really the lineBuffer
          if(findNeedle == NULL)
          {
              strcpy(insertPoint, tmp);
              break;
          }

          // memcpy ensures that no garbage ends up in the lineBuffer. This
          // places the material before the $$ into the buffer then moves
          // the pointer to the next instance.
          memcpy(insertPoint, tmp, findNeedle - tmp);
          insertPoint += findNeedle - tmp;

      
          // Place the PID into the buffer. move the pointer for insertPoint the
          // length of the PID string.
          memcpy(insertPoint, pidString, pidLen);
          insertPoint += pidLen;
          tmp = findNeedle + strlen(needle);
      }
  
      // When the loop is finished, the original line will be replaced with all 
      // instances of the $$ token by the PID of the parent process.
      strcpy(line, lineBuffer);



      // Check if we are in foreground only mode. If we are, remove & from
      // the line.
      if(sigBG == 0)
      {
          token = strtok(line, "&");
          strcpy(line, token);

      }

      fflush(stdout);


/**********************************************************************************
 * Parse the input line for each argument
**********************************************************************************/
      token = strtok(line, " \n");
      numArgs = 0;
      


      while(token != NULL)
      {
         
          // Check for redirection. This redirection sends information from a file
          if(strcmp(token, "<") == 0)
          {
              token = strtok(NULL, " \n");
              inName = strdup(token);
              token = strtok(NULL, " \n");
          }

          // This redirection outputs information to a file.
          else if(strcmp(token, ">") == 0)
          {
              token = strtok(NULL, " \n");
              outName = strdup(token);
              token = strtok(NULL, " \n");
          }

          // This turns on the background flag
          else if(strcmp(token, "&") == 0)
          {
                bg = 1;
                break;
          }

          // Any other input is a comand and will be read as such.
          else
          {
              argInputs[numArgs] = strdup(token);
              token = strtok(NULL, " \n");
              numArgs++;
          }


      }

      // Last argument in the argInputs array must be NULL. Otherwise strange oddities occur.
      argInputs[numArgs] = NULL;

      // If comment, do nothing. Also turn off background flag
      if(argInputs[0] == NULL || strncmp(argInputs[0], "#", 1) == 0)
      {
          bg = 0;
      }


      // Built in cd function
      else if(strcmp(argInputs[0], "cd") == 0)
      {
          // Turn off any background flags
          bg = 0;
          // With no argument, go to the "HOME" dir
          if(argInputs[1] == NULL)
              chdir(getenv("HOME"));
          // Otherwise, go to the specified dir
          else
              chdir(argInputs[1]);
      }

      // Built in status function
      else if(strcmp(argInputs[0], "status") == 0)
      {
          // Turn off background flag
          bg = 0;
          if(WIFEXITED(status))
          {
              fprintf(stdout, "exit status %d\n", WEXITSTATUS(status));
              fflush(stdout);
          }
          else
          {
              fprintf(stdout, "terminated by signal %d\n", status);
              fflush(stdout);
          }

      }

	    
      // Built in exit function
      else if(strcmp(argInputs[0], "exit") == 0)
      {
          // Turn off background flag
          bg = 0;
          // End loop and exit
          exit(0);
          endLoop = 1;
      }


      // If built in function is not used, pass the command to bash
      else
      {
          // Create a child fork
          pid = fork();
          //Initial child should have a PID of zero
          if(pid == 0)
          {
             
              // If we are in the foregrount, the SIGINT will have the default response
              if(!bg)
              {
                  action.sa_handler = SIG_DFL;
                  action.sa_flags = 0;
                  sigaction(SIGINT, &action, NULL);
              }
             
              
              // If the inName variable is used, we will do the following.
              if(inName != NULL)
              {
                  // Open an existing file read only.
                  inFile = open(inName, O_RDONLY);

                  // If there's no file, or the file can't be read, send an error.
                  if(inFile == -1)
                  {
                      fprintf(stderr, "cannot open %s for input\n", inName);
                      fflush(stdout);
                      exit(1);
                  }
		      
                  // Otherwise, read the file. If the read fails, there's a dup2 error.
                  // STDIN_FILENO is required to be 0 per unistd.h in a POSIX system
                  else if(dup2(inFile, STDIN_FILENO) == -1)
                  {
                      fprintf(stderr, "dup2 error\n");
                      fflush(stdout);
                      exit(1);
                  }

                  // Close the file when finished.
                  close(inFile);
              }

              // If we are in background mode, the inFile will be set to /dev/null
              else if(bg)
              {
                  inFile = open("/dev/null", O_RDONLY);

                  if(inFile == -1)
                  {
                      fprintf(stderr, "open error\n");
                      fflush(stdout);
                      exit(1);
                  }

                  else if(dup2(inFile, STDIN_FILENO) == -1)
                  {
                      fprintf(stderr, "dup2 error\n");
                      fflush(stdout);
                      exit(1);
                  }
                  
                  close(inFile);
              }

              // If the outFile is to be used, this happens.
              if(outName != NULL)
              {
                  // Open the output file. If it doesn't exist, create it. If it does exist,
                  // truncate it.
                  outFile = open(outName, O_WRONLY | O_CREAT | O_TRUNC, 0644);

                  // Same error checkers employed by the file input.
                  if(outFile == -1)
                  {
                      fprintf(stderr, "cannot open %s for output\n", inName);
                      fflush(stdout);
                      exit(1);
                  }

                  else if(dup2(outFile, STDOUT_FILENO) == -1)
                  {
                      fprintf(stderr, "dup2 error\n");
                      fflush(stdout);
                      exit(1);
                  }

                  close(outFile);
              }
              
              
              // execvp uses the machine's PATH for program commands.
              if(execvp(argInputs[0], argInputs) == -1)
              {
                  // If the command is not recognized, send an error message and 
                  // set the status to 1. 
                  fprintf(stderr, "Command not recognized: %s\n", argInputs[0]);
                  fflush(stdout);
                  fflush(stderr);
                  status = 1;
                  exit(1);
              }
              

          }

	    
          // If the PID is < 0, there was a problem and the loop will break.
          else if(pid < 0)
          {
              fprintf(stderr, "fork error\n");
              fflush(stdout);
              status = 1;
              break;
          }

          // If the PID is > 0, then another process is active. 
          else
          {
              // If weare in the foreground, we need to wait for the PID to finish
              if(!bg)
              {
                  do
                  {
                      waitpid(pid, &status, WUNTRACED);
                  }while(!WIFEXITED(status) && !WIFSIGNALED(status) );
              }
              // Otherwise we are in the background. Print out the PID of the background
              // process
              else
              {
                  fprintf(stdout, "background pid: %d\n", pid);
                  fflush(stdout);
              }
          }
      
      }
      
      // Before we start the loop, clear out all the arguments.
      for(i = 0; i < numArgs; i++)
      {
          argInputs[i] = NULL;
      }


      // Clean up zombie children
      pid = waitpid(-1, &status, WNOHANG);
      
      // When the PID > 0, a process is in the background.
      while(pid > 0)
      {
          // Print a message when the background process is completed.
          fprintf(stdout, "background pid complete: %d\n", pid);
          fflush(stdout);

          // If a normal exit occurs, print the exit status.
          if(WIFEXITED(status))
          {
              fprintf(stdout, "exit status: %d\n", WEXITSTATUS(status));
              fflush(stdout);
          }
          // Otherwise the process was terminated. Print the terminating signal.
          else
          {
              fprintf(stdout, "Terminating signal: %d\n", status);
              fflush(stdout);
          }

          // Clean up zombie children
          pid = waitpid(-1, &status, WNOHANG);
      }

  }


  return 0; 
}





// This was a function I was intending to use that would count the number of times
// my specific $$ appeared in the line. It became unnecessary, but I want to keep the 
// function here.
  /*
  int numToks=0;
  const char* haystack = line;
  const char* needle = "$$";
  while( (haystack = strstr(haystack, needle)) != NULL)
  {
      haystack += strlen(needle);
      numToks++;
  }
  
  int newSpace = (pidLen - strlen(needle)) * numToks;
  

  char tempLine[strlen(line) + newSpace];
  */


