/**********************************************************************************
 * Name: Clifford Dunn
 * Class: CS 344 - Operating Systems
 * Assignment: Program 3 - smallsh
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <signal.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>


// I am fond of defining macros with the #ifndef because then 
// the compiler can be used to change the value of these macros
#ifndef CHARS_MAX
#define CHARS_MAX 2048
#endif

#ifndef ARG_MAX
#define ARG_MAX 512
#endif


void catchSIGTSTP(int);


int sigBG=1;


void catchSIGTSTP(int sig)
{
    if(sig == SIGTSTP)
    {
      if(sigBG == 0)
      {
          sigBG = 1;
          fprintf(stdout, "Exiting foreground-only mode\n");
          fflush(stdout);
      }
      else if(sigBG == 1)
      {
          sigBG = 0;
          fprintf(stdout, "Entering foreground-only mode (& is now ignored)\n");
          fflush(stdout);
      }
    }
}

/**********************************************************************************
**********************************************************************************/
int main()
{

  int numArgs = 0;
  int endLoop = 0;
  int bg = 0;
  int status = 0;
  int inFile;
  int outFile;
  int i = 0;
  int execErr;
  int pidLen;
  pid_t pid;
  pid_t curPID;
  char line[CHARS_MAX];
  char* argInputs[ARG_MAX];
  char* token;
  char* inName = NULL;
  char* outName = NULL;
  char pidString[50];


  struct sigaction action;
  action.sa_handler = SIG_IGN;
  sigaction(SIGINT, &action, NULL);



  curPID = getpid();
  sprintf(pidString, "%d", (int)curPID);
  pidLen = strlen(pidString);

 
  if(signal(SIGTSTP, catchSIGTSTP) == SIG_ERR)
  {
      fprintf(stderr, "failed to install sighandler\n");
      fflush(stdout);
      exit(1);
  }

  
  while(!endLoop)
  {
      bg = 0;
      
      inName = NULL;
      outName = NULL;

      fprintf(stdout, ": ");
      fflush(stdin);
      fflush(stdout);

      
      fgets(line, sizeof(line), stdin);

      char lineBuffer[2048] = {0};
      char* insert_point = &lineBuffer[0];
      const char* needle = "$$";
      const char* tmp = line;

  
      while(1)
      {
      
          const char* p = strstr(tmp, needle);

      
          if(p == NULL)
          {
              strcpy(insert_point, tmp);
              break;
          }

      
          memcpy(insert_point, tmp, p - tmp);
          insert_point += p - tmp;

      
          memcpy(insert_point, pidString, pidLen);
          insert_point += pidLen;

          tmp = p + strlen(needle);
      }
  
      strcpy(line, lineBuffer);


      if(sigBG == 0)
      {
          token = strtok(line, "&");
          strcpy(line, token);

      }

      fflush(stdout);


      token = strtok(line, " \n");
      numArgs = 0;
      

      while(token != NULL)
      {
      
          if(strcmp(token, "<") == 0)
          {
              token = strtok(NULL, " \n");
              inName = strdup(token);
              token = strtok(NULL, " \n");
          }

          else if(strcmp(token, ">") == 0)
          {
              token = strtok(NULL, " \n");
              outName = strdup(token);
              token = strtok(NULL, " \n");
          }

          else if(strcmp(token, "&") == 0)
          {
                bg = 1;
                break;
          }

          else
          {
              argInputs[numArgs] = strdup(token);
              token = strtok(NULL, " \n");
              numArgs++;
          }


      }

      // Last argument in the argInputs array must be NULL. Otherwise strange oddities occur.
      argInputs[numArgs] = NULL;

      // If comment, do nothing. Also turn off background flag
      if(argInputs[0] == NULL || strncmp(argInputs[0], "#", 1) == 0)
      {
          bg = 0;
      }


      // Built in cd function
      else if(strcmp(argInputs[0], "cd") == 0)
      {
          // Turn off any background flags
          bg = 0;
          // With no argument, go to the "HOME" dir
          if(argInputs[1] == NULL)
              chdir(getenv("HOME"));
          // Otherwise, go to the specified dir
          else
              chdir(argInputs[1]);
      }

      // Built in status function
      else if(strcmp(argInputs[0], "status") == 0)
      {
          // Turn off background flag
          bg = 0;
          if(WIFEXITED(status))
          {
              fprintf(stdout, "exit status %d\n", WEXITSTATUS(status));
              fflush(stdout);
          }
          else
          {
              fprintf(stdout, "terminated by signal %d\n", status);
              fflush(stdout);
          }

      }

	    
      // Built in exit function
      else if(strcmp(argInputs[0], "exit") == 0)
      {
          // Turn off background flag
          bg = 0;
          // End loop and exit
          exit(0);
          endLoop = 1;
      }


      // If built in function is not used, pash the command to bash
      else
      {
          pid = fork();
          if(pid == 0)
          {
             
              if(!bg)
              {
                  action.sa_handler = SIG_DFL;
                  action.sa_flags = 0;
                  sigaction(SIGINT, &action, NULL);
              }
             
              
              if(inName != NULL)
              {
                  inFile = open(inName, O_RDONLY);

                  if(inFile == -1)
                  {
                      fprintf(stderr, "cannot open %s for input", inName);
                      fflush(stdout);
                      exit(1);
                  }
		      
                  // STDIN_FILENO is required to be 0 per unistd.h in a POSIX system
                  else if(dup2(inFile, STDIN_FILENO) == -1)
                  {
                      fprintf(stderr, "dup2 error");
                      fflush(stdout);
                      exit(1);
                  }

                  close(inFile);
              }

              else if(bg)
              {
                  inFile = open("/dev/null", O_RDONLY);

                  if(inFile == -1)
                  {
                      fprintf(stderr, "open error");
                      fflush(stdout);
                      exit(1);
                  }

                  else if(dup2(inFile, STDIN_FILENO) == -1)
                  {
                      fprintf(stderr, "dup2 error");
                      fflush(stdout);
                      exit(1);
                  }
                  
                  close(inFile);
              }

              if(outName != NULL)
              {
                  outFile = open(outName, O_WRONLY | O_CREAT | O_TRUNC, 0644);

                  if(outFile == -1)
                  {
                      fprintf(stderr, "cannot open %s for output", inName);
                      fflush(stdout);
                      exit(1);
                  }

                  else if(dup2(outFile, STDOUT_FILENO) == -1)
                  {
                      fprintf(stderr, "dup2 error");
                      fflush(stdout);
                      exit(1);
                  }

                  close(outFile);
              }
              
              
              if(execvp(argInputs[0], argInputs) == -1)
              {
                  fprintf(stderr, "Command not recognized: %s\n", argInputs[0]);
                  fflush(stdout);
                  status = 1;
                  exit(1);
              }
              

          }

	    
          else if(pid < 0)
          {
              fprintf(stderr, "fork error");
              fflush(stdout);
              status = 1;
              break;
          }

          else
          {
              if(!bg)
              {
                  do
                  {
                      waitpid(pid, &status, WUNTRACED);
                  }while(!WIFEXITED(status) && !WIFSIGNALED(status) );
              }
              else
              {
                  fprintf(stdout, "background pid: %d\n", pid);
                  fflush(stdout);
              }
          }
      
      }
      
      for(i = 0; i < numArgs; i++)
      {
          argInputs[i] = NULL;
      }

      inName = NULL;
      outName = NULL;

      pid = waitpid(-1, &status, WNOHANG);
      
      while(pid > 0)
      {
          fprintf(stdout, "background pid complete: %d\n", pid);
          fflush(stdout);

          if(WIFEXITED(status))
          {
              fprintf(stdout, "exit status: %d\n", WEXITSTATUS(status));
              fflush(stdout);
          }
          else
          {
              fprintf(stdout, "Terminating signal: %d\n", status);
              fflush(stdout);
          }

          pid = waitpid(-1, &status, WNOHANG);
      }

  }


  return 0; 
}






  /*
  int numToks=0;
  const char* haystack = line;
  const char* needle = "$$";
  while( (haystack = strstr(haystack, needle)) != NULL)
  {
      haystack += strlen(needle);
      numToks++;
  }
  
  int newSpace = (pidLen - strlen(needle)) * numToks;
  

  char tempLine[strlen(line) + newSpace];
  */


