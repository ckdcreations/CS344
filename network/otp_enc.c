/******************************************************************************************************************
 * Name: Clifford Dunn
 * Description: OSU CS 344 - OTP Project
 * File Name: otp_enc.c
******************************************************************************************************************/

// Include Libraries
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

// SIZE macro. 75000 is about 10k more than the largest file
#ifndef SIZE
#define SIZE 75000
#endif

// Professor Brewster's error handling. Mine is a bit different.
void error(const char *msg) { perror(msg); exit(0); } // Error function used for reporting issues


/******************************************************************************************************************
 * Function: checkFile
 * Description: Checks the file for bad characters
 * Input: A file name
 * Output: If the file name has no bad characters (capital alpha chars or space), return 1. If there are bad
 *         characters, return 0
******************************************************************************************************************/
int checkFile(char fileIn[])
{
    char c;
    FILE* fileCheck = fopen(fileIn, "r");
    while( (c = fgetc(fileCheck)) != EOF)
    {
        // ASCII table determines that 32 == space, 0 == NULL, 10 == newline
        if(c == 32 || c == 0 || c == 10)
            continue;
        // ASCII table determines capital letters are within these values
        else if(c < 65 || c > 90)
        {
            fclose(fileCheck);
            return 0;
        }

    }
    fclose(fileCheck);
    return 1;
}

/******************************************************************************************************************
 * Function: fileSize
 * Description: Returns an int giving the file size in bytes
 * Input: A file name
 * Output: The file size in bytes
******************************************************************************************************************/
int fileSize(char file[])
{
    FILE* inFile = fopen(file, "r");
    // Go to the end of the file
    fseek(inFile, 0, SEEK_END);
    // Return the current file position which tells us how many bytes are in the file
    int fileSize = ftell(inFile);
    fclose(inFile);
    return fileSize;
}

/******************************************************************************************************************
 * Function: compareFiles
 * Description: Compares the size of the two files
 * Input: Two separate file names
 * Output: If file1 is larger, return 0. If that is not the case, return 1.
******************************************************************************************************************/
int compareFiles(char file1[], char file2[])
{
    
    int msgSize = fileSize(file1);
    int keySize = fileSize(file2);

    if(msgSize > keySize)
        return 0;
    // keySize must be equal to or larger than msgSize 
    else
        return 1;
}
       

/******************************************************************************************************************
 * Function: sendFile
 * Description: Send a file as blocks of text to a remote location
 * Input: A network socket and a file name
 * Output: Data sent across network
******************************************************************************************************************/
void sendFile(int socket, char fileIn[])
{
    // Send data to network location 10 bytes at a time
    // Larger byte sizes should work too. Mileage may vary
    char buffer[10];
    FILE* fileSend = fopen(fileIn, "r");
    memset(buffer, '\0', sizeof(buffer));
    int blockRead, blockWrite;
    int fileSz;

    // fileSz determines how much data should be read. This happens locally, so it should always have
    // the maximum available bytes up to 10
    while( (fileSz = fread(buffer, sizeof(char), 10, fileSend)) > 0)
    {
        // blockWrite sends the data through the socket. If this value is less than zero, break the loop
        if( (blockWrite = write(socket, buffer, fileSz)) < 0)
            break;

        // Zero out the buffer
        memset(buffer, '\0', 10);
    }

    // If blockWrite was exactly 10, send a 0 to nullify the connection at the server
    if(blockWrite == 10)
        write(socket, "0", 1);

    fclose(fileSend);
        

}


/******************************************************************************************************************
 * Function: recvMsg
 * Description: Receive data from a network socket
 * Input: A network socket and the name of a file to write the data
 * Output: A file will be written to the hard drive 
******************************************************************************************************************/
void recvMsg(int socket, char msgFile[10])
{


    // Receive 10 bytes at a time. This matches the value that will be sent from the server
    char inMsg[10];
    memset(inMsg, '\0', 10);

    FILE* tempFile = fopen(msgFile, "a");
    ssize_t fBlock = 0;

    
    // fBlock reads the data from the socket.
    while( (fBlock = read(socket, inMsg, 10)) > 0)
    {
        // Write the data to the file
        fwrite(inMsg, sizeof(char), fBlock, tempFile);

        // When the amount of data sent is less than 10, the whole transmission has been completed.
        if(fBlock < 10)
            break;
    }

    
    fclose(tempFile);

}
            

/******************************************************************************************************************
 * Function: main
 * Description: This is the client program that sends all data to the encryption network server
******************************************************************************************************************/
int main(int argc, char *argv[])
{
	int socketFD, portNumber, charsWritten, charsRead;
	struct sockaddr_in serverAddress;
	struct hostent* serverHostInfo;
	char buffer[SIZE];
    int i = 0;
    
    // Process ID is used for file creation
    int procID = getpid();
    char msgFile[10];
    char keyFile[10];
    
    // Create the name of the files that will be written
    sprintf(msgFile, "%dmsg", procID);
    sprintf(keyFile, "%dkey", procID);


	if (argc < 4) { fprintf(stderr,"USAGE: %s plaintext key port\n", argv[0]); exit(0); } // Check usage & args

    // Make sure the plaintext file for sending has all valid characters
    if(checkFile(argv[1]) == 0)
    {
        fprintf(stderr, "Error: File '%s' has bad characters\n", argv[1]);
        exit(0);
    }

    // Make sure the key file is large enough for encryption
    if(compareFiles(argv[1], argv[2]) == 0)
    {
        fprintf(stderr, "Error: key '%s' is too short\n", argv[2]);
        exit(0);
    }

    
    
    /**********************************************************************
     * The following code is from the final lectures for CS 344.
     * Thanks Professer Brewster!
    **********************************************************************/
     
	// Set up the server address struct
	memset((char*)&serverAddress, '\0', sizeof(serverAddress)); // Clear out the address struct
	portNumber = atoi(argv[3]); // Get the port number, convert to an integer from a string
	serverAddress.sin_family = AF_INET; // Create a network-capable socket
	serverAddress.sin_port = htons(portNumber); // Store the port number
	serverHostInfo = gethostbyname("localhost"); // Convert the machine name into a special form of address
	if (serverHostInfo == NULL) { fprintf(stderr, "CLIENT: ERROR, no such host\n"); exit(0); }
	memcpy((char*)&serverAddress.sin_addr.s_addr, (char*)serverHostInfo->h_addr, serverHostInfo->h_length); // Copy in the address

	// Set up the socket
	socketFD = socket(AF_INET, SOCK_STREAM, 0); // Create the socket
	if (socketFD < 0) error("CLIENT: ERROR opening socket");
	
	// Connect to server
	if (connect(socketFD, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) < 0) // Connect socket to address
		error("CLIENT: ERROR connecting");
    /***********************************************************************/
    
    // Send a message to make sure we are communicating with the enc server
    memset(buffer, '\0', sizeof(buffer));
    char serverType[] = "enc";
    
    // Send the message "enc" to ensure we are communicating with the encode server
    write(socketFD, serverType, sizeof(serverType));

    // Receive the message "enc_d" to signify we're connected to the right server
    read(socketFD, buffer, sizeof(buffer));
    if(strcmp(buffer, "enc_d") != 0)
    {
        fprintf(stderr, "Error: otp_enc can't use otp_dec_d\n");
        exit(0);
    }
    else
    {
        


        // Send the plaintext file
        sendFile(socketFD, argv[1]);
        
        // Wait for confirmation from the server that the plaintext file was sent. 
        // This doesn't seem vital, but there were errors before adding this fragment
        char confirm[2];
        memset(confirm, '\0', sizeof(confirm));

        read(socketFD, confirm, sizeof(confirm));
        // When the confirmation has been sent, send the keyfile
        if(strcmp(confirm, "1") == 0)
        {
            sendFile(socketFD, argv[2]);
        }


        // Receive the msgFile. This will be the encrypted version of the plaintext file
        recvMsg(socketFD, msgFile);
        
        // Print out the file that was received. Read from the hard drive and print char by char
        // to stdout.
        char c;
        FILE* fileCheck = fopen(msgFile, "r");
        while( (c = fgetc(fileCheck)) != EOF)
        {
            fprintf(stdout, "%c", c);
        }

        fclose(fileCheck);
   

      

        
    }
        

	close(socketFD); // Close the socket
    // Remove the two files that were created from the hard drive.
    unlink(msgFile);
    unlink(keyFile);
    
	return 0;
}
