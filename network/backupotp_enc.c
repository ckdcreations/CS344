#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#ifndef SIZE
#define SIZE 75000
#endif

void error(const char *msg) { perror(msg); exit(0); } // Error function used for reporting issues

int checkFile(char fileIn[])
{
    char c;
    FILE* fileCheck = fopen(fileIn, "r");
    while( (c = fgetc(fileCheck)) != EOF)
    {
        if(c == 32 || c == 0 || c == 10)
            continue;
        else if(c < 65 || c > 90)
            return 0;

    }
    return 1;
}

int fileSize(char file[])
{
    FILE* inFile = fopen(file, "r");
    fseek(inFile, 0, SEEK_END);
    int fileSize = ftell(inFile);
    fclose(inFile);
    return fileSize;
}

int compareFiles(char file1[], char file2[])
{
    
    int msgSize = fileSize(file1);
    int keySize = fileSize(file2);

    if(msgSize > keySize)
        return 0;
    else
        return 1;
}
       
void sendFile(int socket, char fileIn[])
{
    char buffer[10];
    FILE* fileSend = fopen(fileIn, "r");
    memset(buffer, '\0', sizeof(buffer));
    int blockRead, blockWrite;
    int fileSz;

    while( (fileSz = fread(buffer, sizeof(char), 10, fileSend)) > 0)
    {
        if( (blockWrite = write(socket, buffer, fileSz)) < 0)
            break;

        memset(buffer, '\0', 10);
    }

    if(blockWrite == 10)
        write(socket, "0", 1);

    fclose(fileSend);
        


    /*
    while(fileSz > 0)
    {
        blockRead = fread(buffer, sizeof(char), SIZE, fileSend);
        if(blockRead == 0)
            break;

        fileSz -= blockRead;
    }

    char* pnt;
    pnt = buffer;
    while(blockRead > 0)
    {
        blockWrite = write(socket, pnt, blockRead);
        if(blockWrite < 0)
        {
            fprintf(stderr, "Error writing to socket");
            exit(0);
        }
        blockRead -= blockWrite;
        pnt += blockWrite;
    }

    */


}

            

int main(int argc, char *argv[])
{
	int socketFD, portNumber, charsWritten, charsRead;
	struct sockaddr_in serverAddress;
	struct hostent* serverHostInfo;
	char buffer[SIZE];
    int i = 0;
    
	if (argc < 4) { fprintf(stderr,"USAGE: %s port plaintext key\n", argv[0]); exit(0); } // Check usage & args

    if(checkFile(argv[2]) == 0)
    {
        fprintf(stderr, "Error: File has bad characters\n");
        exit(0);
    }

    if(compareFiles(argv[2], argv[3]) == 0)
    {
        fprintf(stderr, "Error: key '%s' is too short\n", argv[3]);
        exit(0);
    }

    
    
	// Set up the server address struct
	memset((char*)&serverAddress, '\0', sizeof(serverAddress)); // Clear out the address struct
	portNumber = atoi(argv[1]); // Get the port number, convert to an integer from a string
	serverAddress.sin_family = AF_INET; // Create a network-capable socket
	serverAddress.sin_port = htons(portNumber); // Store the port number
	//serverHostInfo = gethostbyname(argv[1]); // Convert the machine name into a special form of address
	serverHostInfo = gethostbyname("localhost"); // Convert the machine name into a special form of address
	if (serverHostInfo == NULL) { fprintf(stderr, "CLIENT: ERROR, no such host\n"); exit(0); }
	memcpy((char*)&serverAddress.sin_addr.s_addr, (char*)serverHostInfo->h_addr, serverHostInfo->h_length); // Copy in the address

	// Set up the socket
	socketFD = socket(AF_INET, SOCK_STREAM, 0); // Create the socket
	if (socketFD < 0) error("CLIENT: ERROR opening socket");
	
	// Connect to server
	if (connect(socketFD, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) < 0) // Connect socket to address
		error("CLIENT: ERROR connecting");

    memset(buffer, '\0', sizeof(buffer));
    char serverType[] = "enc";
    
    write(socketFD, serverType, sizeof(serverType));
    read(socketFD, buffer, sizeof(buffer));
    if(strcmp(buffer, "enc_d") != 0)
    {
        fprintf(stderr, "Error: wrong server type\n");
        exit(0);
    }
    else
    {
        char c;
        char msgBack[fileSize(argv[2])];
        i = 0;
        memset(buffer, '\0', sizeof(buffer));
        /*
        FILE* msgFile = fopen(argv[2], "r");
        while( (c = fgetc(msgFile)) != EOF)
        {
            buffer[i] = c;
            i++;
        }
        */

    
        sendFile(socketFD, argv[2]);
        
        /*
        read(socketFD, buffer, sizeof(buffer));
        sprintf(msgBack, "%s\0", buffer);
    
        fprintf(stdout, "%s", msgBack);
        */
        
    }
        



    /*
	// Get input message from user
	memset(buffer, '\0', sizeof(buffer)); // Clear out the buffer array
	fgets(buffer, sizeof(buffer) - 1, stdin); // Get input from the user, trunc to buffer - 1 chars, leaving \0
	buffer[strcspn(buffer, "\n")] = '\0'; // Remove the trailing \n that fgets adds

	// Send message to server
	charsWritten = send(socketFD, buffer, strlen(buffer), 0); // Write to the server
	if (charsWritten < 0) error("CLIENT: ERROR writing to socket");
	if (charsWritten < strlen(buffer)) printf("CLIENT: WARNING: Not all data written to socket!\n");

	// Get return message from server
	memset(buffer, '\0', sizeof(buffer)); // Clear out the buffer again for reuse
	charsRead = recv(socketFD, buffer, sizeof(buffer) - 1, 0); // Read data from the socket, leaving \0 at end
	if (charsRead < 0) error("CLIENT: ERROR reading from socket");
	printf("CLIENT: I received this from the server: \"%s\"\n", buffer);

    */

	close(socketFD); // Close the socket
    
	return 0;
}
