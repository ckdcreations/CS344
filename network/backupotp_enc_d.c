/*******************************************************************************************************************
 * Name: Clifford Dunn
*******************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#ifndef SIZE
#define SIZE 75000
#endif

static const char* chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
void error(const char *msg) { perror(msg); exit(1); } // Error function used for reporting issues

char intToChar(int convert)
{

    if(convert > 0 && convert < 27)
        return chars[convert];
    else
        return 'z';
}


int charToInt(char convert)
{
    int i;
    for(i = 0; i < 27; i++)
    {
        if(convert == chars[i])
            return i;
    }

    return -1;
}

int recvMsg(int socket, int procID)
{
    
    char fName[10];
    memset(fName, '\0', 10);
    char inMsg[10];
    memset(inMsg, '\0', 10);
    sprintf(fName, "%dfile", procID);
    
    FILE* tempFile = fopen(fName, "a");
    int fBlock = 0;
    while(fBlock = recv(socket, inMsg, 10, 0) > 0)
    {
        int writeBlock = fwrite(inMsg, sizeof(char), fBlock, tempFile);
        fprintf(stdout, "%s\n", inMsg);
        if(fBlock < 10)
            break;
    }

    fclose(tempFile);
    
    FILE* countFile = fopen(fName, "r");
    fseek(countFile, 0, SEEK_END);
    int charNum = ftell(countFile);
    printf("recvd");
    return charNum;
}

int main(int argc, char *argv[])

{
	int listenSocketFD, establishedConnectionFD, portNumber, inMsgSize, inKeySize;
	socklen_t sizeOfClientInfo;
	char buffer[SIZE];
	struct sockaddr_in serverAddress, clientAddress;
    pid_t chPID;
    int i = 0;

	if (argc < 2) { fprintf(stderr,"USAGE: %s port\n", argv[0]); exit(1); } // Check usage & args

	// Set up the address struct for this process (the server)
	memset((char *)&serverAddress, '\0', sizeof(serverAddress)); // Clear out the address struct
	portNumber = atoi(argv[1]); // Get the port number, convert to an integer from a string
	serverAddress.sin_family = AF_INET; // Create a network-capable socket
	serverAddress.sin_port = htons(portNumber); // Store the port number
	serverAddress.sin_addr.s_addr = INADDR_ANY; // Any address is allowed for connection to this process

	// Set up the socket
	listenSocketFD = socket(AF_INET, SOCK_STREAM, 0); // Create the socket
	if (listenSocketFD < 0) error("ERROR opening socket");

	// Enable the socket to begin listening
	if (bind(listenSocketFD, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) // Connect socket to port
		error("ERROR on binding");
	listen(listenSocketFD, 5); // Flip the socket on - it can now receive up to 5 connections

    // Accept a connection, blocking if one is not available until one connects
    sizeOfClientInfo = sizeof(clientAddress); // Get the size of the address for the client that will connect
   
    while(1)
    {
        establishedConnectionFD = accept(listenSocketFD, (struct sockaddr *)&clientAddress, &sizeOfClientInfo); // Accept
        if (establishedConnectionFD < 0) error("ERROR on accept");

        chPID = fork();
        if(chPID < 0)
        {
            fprintf(stderr, "Server: ERROR while forking process");
            exit(1);
        }

        if(chPID == 0)
        {
            memset(buffer, '\0', sizeof(buffer));
            int totalBytes = SIZE;
            int bytesRead = 0;
            char* pnt = buffer;

            read(establishedConnectionFD, buffer, sizeof(buffer)-1);
            if(strcmp(buffer, "enc") == 0)
            {
                char serverType[] = "enc_d";
                write(establishedConnectionFD, serverType, sizeof(serverType));
            }
            else
            {
                char wrongServer[] = "no";
                write(establishedConnectionFD, wrongServer, sizeof(wrongServer));
            }


            memset(buffer, '\0', sizeof(buffer));

            recvMsg(establishedConnectionFD, chPID);
            
            /*
            printf("start");
            int loop = 1;
            while(loop)
            {
                bytesRead = read(establishedConnectionFD, pnt, totalBytes);
                if(bytesRead == 0)
                    loop = 0;
                if(bytesRead < 0)
                    fprintf(stderr, "Server: Error reading from socket");
                
                pnt += bytesRead;
                totalBytes -= bytesRead;
            }
            printf("done");
            */
            


            /*
            char returnMsg[SIZE];
            memset(returnMsg, '\0', SIZE);

            strcpy(returnMsg, buffer);


            write(establishedConnectionFD, returnMsg, sizeof(returnMsg));
            */


            
            /*
            int numChars = recvMsg(listenSocketFD, chPID);
            */
            /*
            char conf[2];
            memset(conf, '\0', 2);
            sprintf(conf, "1");
            write(listenSocketFD, conf, strlen(conf));
            */
            /*
            char fName[10];
            sprintf(fName, "%dfile", chPID);

            char sendBack[numChars - 1];
            memset(sendBack, '\0', numChars-1);
            char c;
            FILE* readFile = fopen(fName, "r");

            while( (c = fgetc(readFile)) != EOF && i < numChars-1)
            {
                sendBack[i] = c;
                i++;
            }
            sprintf(sendBack, "%s\0", sendBack);
            write(listenSocketFD, sendBack, numChars-1 );


            exit(0);
            */
        }



        close(establishedConnectionFD); // Close the existing socket which is connected to the client
    }
        
    close(listenSocketFD); // Close the listening socket

	return 0; 
}
