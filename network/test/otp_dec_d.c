/*******************************************************************************************************************
 * Name: Clifford Dunn
 * Description: OSU CS344 - OTP Project
 * File Name: otp_dec_d.c
*******************************************************************************************************************/

// Include Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

// SIZE macro. 75000 is about 10k more than the largest file
#ifndef SIZE
#define SIZE 75000
#endif

// Static value for allLetters. The alphabet plus space that will be needed for encryption
static const char* allLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
// Professor Brewster's error handling. Mine is a bit different
void error(const char *msg) { perror(msg); exit(1); } // Error function used for reporting issues


/*
char intToChar(int convert)
{

    if(convert > 0 && convert < 27)
        return allLetters[convert];
    else
        return 'z';
}


int charToInt(char convert)
{
    int i;
        
    if(convert == allLetters[27])
        return 32;

    for(i = 0; i < 27; i++)
    {
        if(convert == allLetters[i])
            return i;
    }

    return -1;
}
*/

/******************************************************************************************************************
 * Function: sendFile
 * Description: Send a file as blocks of text to a remote location
 * Input: A network socket and a file name
 * Output: Data sent across network
*******************************************************************************************************************/
void sendFile(int socket, char fileIn[])
{
    // Send data to a network location 10 bytes at a time
    // Larger byte sizes should work too. Mileage may vary
    char buffer[10];
    FILE* fileSend = fopen(fileIn, "r");
    memset(buffer, '\0', sizeof(buffer));
    int blockRead, blockWrite;
    int fileSz;

    // fileSz determines how much data should be read. This happens locally, so it should always have
    // the maximum available bytes up to 10
    while( (fileSz = fread(buffer, sizeof(char), 10, fileSend)) > 0)
    {
        if( (blockWrite = write(socket, buffer, fileSz)) < 0)
            break;

        // Zero out the buffer
        memset(buffer, '\0', 10);
    }
    
   
    // If blockWrite was exactly 10, send a 0 to nullify the connection at the client
    if(blockWrite == 10)
        write(socket, "0", 1);

   
    fclose(fileSend);
}


/******************************************************************************************************************
 * Function: recvMsg
 * Description: Receive data from a network socket
 * Input: A network socket and the name of a file to write the data
 * Output: A file will be written to the hard drive. The size of the file will be returned
********************************************************************************************************************/
int recvMsg(int socket, char msgFile[10])
{
    
    // Receive 10 bytes at a time. This matches the value that will be sent from the client
    char inMsg[10];
    memset(inMsg, '\0', 10);
    
    FILE* tempFile = fopen(msgFile, "w");
    ssize_t fBlock = 0;


    // fBlock reads the data from the socket.
    while( (fBlock = read(socket, inMsg, 10)) > 0)
    {
        // Write the data to the file
        fwrite(inMsg, sizeof(char), fBlock, tempFile);

        // When the amount of data sent is less than 10, the whole transmission has been completed.
        if(fBlock < 10)
            break;
    }

    fclose(tempFile);
    FILE* countFile = fopen(msgFile, "r");
    fseek(countFile, 0, SEEK_END);
    int numChars = ftell(countFile);

    // Return the size of the file
    return numChars;
    
}

/******************************************************************************************************************
 * Function: recvKey
 * Description: Receive data from a network socket
 * Input: A network socket and the name of a file to write the data
 * Output: A file will be written to the hard drive.
********************************************************************************************************************/
void recvKey(int socket, char keyFile[10])
{
    // Receive 10 bytes at a time. This matches the value that will be sent from the client
    char inKey[10]; 
    memset(inKey, '\0', 10);

    FILE* key = fopen(keyFile, "w");
    int fBlock = 0;

    // fBlock reads the data from the socket.
    while(fBlock = recv(socket, inKey, 10, 0) > 0)
    {
        // Write the data to the file
        fwrite(inKey, sizeof(char), fBlock, key);

        // When the amount of data sent is less than 10, the wole transmission has been completed.
        if(fBlock < 10)
            break;
    }

    fclose(key);
    return;
}

 
/******************************************************************************************************************
 * Function: decode 
 * Descripion: Decrypt the message file with a cipher key
 * Input: The message file name, the key file name, the decrypt file name, and the number of chars in the message
 * Output: The decryption file will contain the original message decrypted 
*******************************************************************************************************************/
void decode(char msgFile[10], char keyFile[10], char decFile[10], int numChars)
{
    // Open all three files
    FILE* msg = fopen(msgFile, "r");
    FILE* key = fopen(keyFile, "r");
    FILE* dec = fopen(decFile, "w");

    // Create arrays to hold the files' data
    char msgArray[numChars];
    char keyArray[numChars];
    char decArray[numChars];

    memset(decArray, '\0', sizeof(decArray));

    // Counter and char variables
    int i = 0, j = 0;
    // Even though c and d represent char, we will read them as int for purpose of decryption
    int c, d;

    // Loop through the msgFile and place all characters into an array
    while( (c = fgetc(msg)) != EOF && i < numChars-1)
    {
        msgArray[i] = c;
        i++;
    }

    fclose(msg);

    // Loop through the keyFile and place all characters into an array
    while( (d = fgetc(key)) != EOF && j < numChars-1)
    {
        keyArray[j] = d;
        j++;
    }

    fclose(key);

    // Loop through all the characters that are in the message. Encrypt them with the key
    for(i = 0; i < numChars-1; i++)
    {
        int msgVal = 0;
        int keyVal = 0;
        int moduloRem; 
        // For each character in the message, find the appropriate letter
        for(j = 0; j < 28; j++)
        {
            // From the encrypted message, find the number of the appropriate char
            if(msgArray[i] == allLetters[j])
                msgVal = j;
            // From the cipher key, find the number of the appropriate char
            if(keyArray[i] == allLetters[j])
                keyVal = j;
        }

        // Subtract the value of the key from the value of the msg char
        int sum = msgVal - keyVal;
        // If the resultant sum is less than zero, add 27 to the value
        if(sum < 0)
            moduloRem = sum + 27;
        // Otherwise use the value as is.
        else
            moduloRem = sum;

        decArray[i] = allLetters[moduloRem];
    }

    // Make sure the last value of the decArray is a newline symbol
    decArray[numChars-1] = '\n';
    // Write the entire decArray to the encrypted file
    fwrite(decArray, sizeof(char), sizeof(decArray), dec);


    fclose(dec);

    

}

/******************************************************************************************************************
 * Function: main
 * Description: This is the server program that sends all data to the decryption network client
********************************************************************************************************************/
int main(int argc, char *argv[])

{
	int listenSocketFD, socketFD, portNumber, inMsgSize, inKeySize;
	socklen_t sizeOfClientInfo;
	char buffer[SIZE];
	struct sockaddr_in serverAddress, clientAddress;
    // The child process ID will be used for file creation
    pid_t chPID;
    // Counter variable
    int i = 0;

	if (argc < 2) { fprintf(stderr,"USAGE: %s port\n", argv[0]); exit(1); } // Check usage & args

    /**********************************************************************
     * The following code is from the final lectures for CS 344.
     * Thanks Professer Brewster!
    **********************************************************************/

	// Set up the address struct for this process (the server)
	memset((char *)&serverAddress, '\0', sizeof(serverAddress)); // Clear out the address struct
	portNumber = atoi(argv[1]); // Get the port number, convert to an integer from a string
	serverAddress.sin_family = AF_INET; // Create a network-capable socket
	serverAddress.sin_port = htons(portNumber); // Store the port number
	serverAddress.sin_addr.s_addr = INADDR_ANY; // Any address is allowed for connection to this process

	// Set up the socket
	listenSocketFD = socket(AF_INET, SOCK_STREAM, 0); // Create the socket
	if (listenSocketFD < 0) error("ERROR opening socket");

	// Enable the socket to begin listening
	if (bind(listenSocketFD, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) // Connect socket to port
		error("ERROR on binding");
	listen(listenSocketFD, 5); // Flip the socket on - it can now receive up to 5 connections

    // Accept a connection, blocking if one is not available until one connects
    sizeOfClientInfo = sizeof(clientAddress); // Get the size of the address for the client that will connect
    /***********************************************************************/
   
    // The while loop will keep the server alive forever. It will require a signal command to quit.
    while(1)
    {
        socketFD = accept(listenSocketFD, (struct sockaddr *)&clientAddress, &sizeOfClientInfo); // Accept
        if (socketFD < 0) error("ERROR on accept");

        // When a new connection is made, fork to a new process. This way, the server can keep taking new connections
        chPID = fork();
        if(chPID < 0)
        {
            fprintf(stderr, "Server: ERROR while forking process");
            exit(1);
        }

        if(chPID == 0)
        {
            // Zero out the buffer and allocate new variables as necessary
            memset(buffer, '\0', sizeof(buffer));
            int totalBytes = SIZE;
            int bytesRead = 0;
            char* pnt = buffer;
         
            int procID = getpid();
            char msgFile[10];
            char keyFile[10];
            char decFile[10];

                
            // Create the names for files that will be created throughout.
            sprintf(msgFile, "%dmsg", procID);
            sprintf(keyFile, "%dkey", procID);
            sprintf(decFile, "%ddec", procID);
            


            // Receive the command from the network client. Make sure it is from the dec client
            read(socketFD, buffer, sizeof(buffer)-1);
            // If the wrong client connects, send a message back to the client that they are using the wrong
            // server
            if(strcmp(buffer, "dec") != 0)
            {
                char wrongServer[] = "no";
                write(socketFD, wrongServer, sizeof(wrongServer));
            }
                        
            // Otherwise, this is the correct server
            else
            {
                // Send the correct data to the client
                int numChars;
                char serverType[] = "dec_d";
                char confirm[2];
                memset(confirm, '\0', sizeof(confirm));
                write(socketFD, serverType, sizeof(serverType));
            
                memset(buffer, '\0', sizeof(buffer));

                           
                // Receive the plaintext file
                numChars = recvMsg(socketFD, msgFile);
                // Send receipt confirmation
                sprintf(confirm, "1");
                write(socketFD, confirm, sizeof(confirm));
                // Receive the keyfile
                recvMsg(socketFD, keyFile);
            

                // Decrypt the msgFile
                decode(msgFile, keyFile, decFile, numChars);
        

                // Send the decrypted message file back to the client
                sendFile(socketFD, decFile);

                // Close the socket
                close(socketFD); // Close the existing socket which is connected to the client


                // Remove all created files from the hard drive
                unlink(msgFile);
                unlink(keyFile);
                unlink(decFile);
            }
        }



    }
        
    close(listenSocketFD); // Close the listening socket

	return 0; 
}
