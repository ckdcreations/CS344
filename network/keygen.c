/**********************************************************************************
 * Name: Clifford Dunn
 * Description: OSU CS 344 - OTP Project
 * File Name: keygen.c
**********************************************************************************/

// Include Libraries
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <signal.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>


int main(int argc, char* argv[])
{

    // Check for correct usage
    if(argc < 2)
    {
        fprintf(stderr, "USAGE: %s numChars\n", argv[0]);
        exit(0);
    }
    // Seed the random values
    srand(time(0));
    // Create a counter
    int i;
    // Use the value input to create the number of characters
    int numChars = atoi(argv[1]);
    // Create the nextChar variable for holding the next char
    char nextChar;

    // For loop to generate enough random values as requested.
    for(i = 0; i < numChars; i++)
    {
        // Generate a random value from 0 to 26. Add 65 to that
        nextChar = (rand() % 27) + 65;
        // If the nextChar == 91, create a space
        if(nextChar == 91)
            nextChar = 32;

        // Print the nextChar out
        fprintf(stdout, "%c", nextChar);

    }
    // At the end, add a nextline char to the value
    fprintf(stdout, "\n");

    return 0;
}
